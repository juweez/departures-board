var departuresForm = document.querySelector('.js-departures-form');

function getDepartures(station) {
  var xhr = new XMLHttpRequest();

  xhr.open('GET', '/search?name='+station, true);
  xhr.send();

  xhr.onload = function() {
    if (xhr.status >= 200 && xhr.status < 400) {
      document.getElementById('departures-list').innerHTML = xhr.responseText;
      addToLocalStorage(station);
    } else {
      document.getElementById('departures-list').innerHTML = 'No matching station';
    }
  }
}

function addToLocalStorage(station) {
  var storedStations = JSON.parse(localStorage.getItem('stations')) || [];
  storedStations.unshift(station);
  localStorage.setItem('stations', JSON.stringify(storedStations));
  updatePreviousSearches();
}

function updatePreviousSearches() {
  var storedStations = JSON.parse(localStorage.getItem('stations')).slice(0, 10);
  document.getElementById('previous-searches').innerHTML = '';

  storedStations.forEach(function(station) {
    var item = document.createElement('li'),
        itemLink = document.createElement('a');

    itemLink.href = '/search?name='+station;
    itemLink.classList = 'js-search-station';
    itemLink.appendChild(document.createTextNode(station));
    itemLink.addEventListener('click', function(e) {
      e.preventDefault();
      getDepartures(station);
    });

    item.appendChild(itemLink);
    document.getElementById('previous-searches').append(item);
  });
}

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

departuresForm.addEventListener('submit', function(e) {
  e.preventDefault();

  var stationInputValue = document.querySelector('.js-station-input').value;

  if (stationInputValue) {
    getDepartures(stationInputValue);
  }
});

ready(function(){
  if (localStorage.getItem('stations')) {
    updatePreviousSearches();
  }
});
