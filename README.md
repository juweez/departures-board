
# StreetTeam Code Task
#### Introduction

This task aims to test a developers competency with the core front-end technologies: HTML, CSS and JavaScript.

The task should take around 2 hours to complete, but feel free to spend longer or shorter on it if you like. You should not make use of any front-end framework such as Twitter Bootstrap, React or jQuery.

After the task we will talk through your code, questioning decisions you've made and how you would go about extending your application.

#### Pre-Requisites

- Ensure you're running Node 6
- Run `npm install` inside the directory
- Copy the `.env.example` file to `.env` and store your own [Huxley](https://huxley.apphb.com/) access token.

#### Instructions

- Run `npm run dev` to serve the application and watch scripts for changes
- Run `npm run unit` to run unit tests defined by `*.spec.js`
- Run `npm run test` to run functional tests in the `tests/` folder

Functional tests are written with TestCafé, you can [read more about the API](https://devexpress.github.io/testcafe/documentation/test-api/).

## Task
#### Build a departures board

Consume [Huxley](https://huxley.apphb.com/) to make a departures board for train stations across the UK. The board must be suitable for reading on a mobile and desktop device. You are free to handle the API however you like, on the client or server.

The departures board must contain: destination, train operator, departure time, platform, delays and anything else you think would be useful to consumers.

We want to be able to store a user's previous searches for departures and display them in our application. Do this however you want: LocalStorage, IndexedDB, MongoDB, Redis, etc.

We have added some *very basic* CSS to create a "theme" for the application, so you won't need to do this. Concentrate your effort on making the app responsive and employing mobile-first development techniques.

The application comes with functional and unit testing frameworks that you are encouraged to make use of, we have also configured Express and Mustache. Make use of these tools or extend them if you wish.


#### On completion

Archive the application folder excluding the `node_modules` folder and return it to your point of contact.
