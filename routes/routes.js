
const express = require('express'),
      server = express(),
      index = require('./index/index');

server.get('/', index.index);
server.get('/search', index.search);

module.exports = server;
