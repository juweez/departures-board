var request = require('request');

const index = (req, res) => {
  res.render('master', {
    head: { title: 'StreetTeam Front-End Task' }
  });
}

const search = (req, res) => {
  request('https://huxley.apphb.com/departures/'+req.query.name+'?accessToken='+process.env.HUXLEY_TOKEN, function (err, response, body) {
    if (err || response.statusCode !== 200) {
      return res.sendStatus(500);
    }

    var data = JSON.parse(body),
        stationName = data.locationName,
        formattedData = [];

    if (data.trainServices) {
      data.trainServices.forEach(function(trainService) {
        var statusClass = (trainService.etd == 'On time') ? 'u-text--success' : 'u-text--danger';
        var service = {
          destination: trainService.destination[0].locationName,
          operator: trainService.operator,
          scheduledTime: trainService.std,
          expectedTime: trainService.etd,
          platform: trainService.platform || '–',
          statusClass: statusClass
        };

        formattedData.push(service);
      });

      res.render('departures', {
        head: { title: 'StreetTeam Front-End Task' },
        stationName: stationName,
        trainServices: formattedData
      });
    }

    else {
      res.send('No departures available');
    }
  });
}

module.exports = {
  index,
  search
};
