
const expect = require('chai').expect,
      Selector = require('testcafe').Selector,
      getElement = Selector(sel => document.querySelector(sel));

fixture('index').page('http://127.0.0.1:5000');

test('should contain a heading containing the task title', () => {
  getElement('h1').then(value => {
    expect(value).to.equal('Train Departures');
  });
});

test('should contain a heading containing the previous searches title', () => {
  getElement('h2').then(value => {
    expect(value).to.equal('Previous searches');
  });
});
